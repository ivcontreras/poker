# poker

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

## Instalar Node JS

[Enlace de descarga](https://nodejs.org/es/download/)

## Instalar aplicación

En la carpeta del proyecto clonado ejecutar mediante la línea de comandos lo siguiente:

+ `npm install`  
+ `npm install bower`  
+ `npm install -g grunt-cli`  
+ `npm install grunt`
+ `npm install yo`
+ `npm install generator-karma`
+ `npm install generator-angular`

## Ejecutar proyecto

En la carpeta del proyecto clonado ejecutar mediante la línea de comandos:

+ `grunt serve`

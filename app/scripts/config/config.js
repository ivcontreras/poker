'use strict';


angular.module('pokerApp')

  	.config(function ($stateProvider, $locationProvider, $urlRouterProvider) {

  	$urlRouterProvider.otherwise('/poker/jugar');
    $stateProvider
	    .state('base', {
	        abstract: true,
	        template: '<ui-view/>',

	    })
	    .state('base.poker', {
	        abstract: true,
	        url: '/poker',
	        templateUrl: 'views/base.html',
	        controller: 'BaseCtrl',

	    })
	    .state('base.poker.play', {
	        url: '/jugar',
	        templateUrl: 'views/base/play.html',
	        controller: 'BaseNewCtrl',

	    })
  	
  		$locationProvider.hashPrefix('');

	}).config(function(RestangularProvider, ENVIRONMENT) {
		RestangularProvider.setBaseUrl(ENVIRONMENT.baseUrl);
		RestangularProvider.setPlainByDefault(true);
  	});
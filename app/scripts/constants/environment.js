'use strict';


angular.module('pokerApp')
    .constant('ENVIRONMENT', {

        baseUrl: 'https://services.comparaonline.com',

        headers: {
        	'Accept': '*/*',
			'Accept-Encoding': 'gzip, deflate',
			'Connection': 'keep-alive',
			'Content-Length': '0',
			'Host': 'services.comparaonline.com',
			'User-Agent': 'HTTPie/0.9.9'
        },

        handsRanking: {
        	'HIGH_CARD': {name: 'Hight Card', ranking: 1 },
        	'ONE_PAIR': {name: 'One Pair', ranking: 2 },
        	'TWO_PAIRS': {name: 'Two Pairs', ranking: 3 },
        	'THREE_OF_A_KIND': {name: 'Three of a kind', ranking: 4 },
        	'STRAIGHT': {name: 'Straigth', ranking: 5 },
        	'FLUSH': {name: 'Flush', ranking: 6 },
        	'FULL_HOUSE': {name: 'Full House', ranking: 7 },
        	'FOUR_OF_A_KIND': {name: 'Four of a kind', ranking: 8 },
        	'STRAIGHT_FLUSH': {name: 'Straigth Flush', ranking: 9 },
        	'ROYAL_FLUSH': {name: 'Royal Flush', ranking: 10 }
        }

});
'use strict';

/**
 * @ngdoc overview
 * @name pokerApp
 * @description
 * # pokerApp
 *
 * Main module of the application.
 */
angular
  .module('pokerApp', [
    'ngAnimate',
    'ngResource',
    'ngSanitize',
    'ui.router',
    'restangular'
  ]);

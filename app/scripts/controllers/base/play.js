'use strict';


angular.module('pokerApp')
  	.controller('BaseNewCtrl', function($rootScope, $scope, Restangular, ENVIRONMENT) {

  	$scope.getCards = function(index, cardsNumber) {
  		Restangular.all('/dealer/deck/'+$scope.token+'/deal/'+cardsNumber).getList({})
        	.then(function(response) {
                $scope.players[0].cards.push(response.slice(0, 5));
                $scope.players[1].cards.push(response.slice(5, 11));            
                return;

        }, function(response) {
        	console.log("Error en comunicarse con el servicio");
            if(response.status != 405) {
                $scope.getCards(0, cardsNumber);
            } else {
                console.log("Se han acabado las cartas");
                $scope.noMoreCards = true;
            }            

        });         
  	};

    $scope.more = function() {
        $scope.number++;
        if($scope.number < 8){
            $scope.indexes = new Array($scope.number);
        }
    };

    $scope.winner = function(index){
        var player1 = angular.copy($scope.players[0]);
        var player2 = angular.copy($scope.players[1]);

        if((isRoyalFlush(player1.cards[index]) && isRoyalFlush(player2.cards[index])) || (!isRoyalFlush(player1.cards[index]) && !isRoyalFlush(player2.cards[index]))) {
            if((isStraightFlush(player1.cards[index]) && isStraightFlush(player2.cards[index])) || (!isStraightFlush(player1.cards[index]) && !isStraightFlush(player2.cards[index]))) {
                if((isFourOfKind(player1.cards[index]) && isFourOfKind(player2.cards[index])) || (!isFourOfKind(player1.cards[index]) && !isFourOfKind(player2.cards[index]))) {
                    if((isFullHouse(player1.cards[index]) && isFullHouse(player2.cards[index])) || (!isFullHouse(player1.cards[index]) && !isFullHouse(player2.cards[index]))) {
                        if((isFlush(player1.cards[index]) && isFlush(player1.cards[index])) || (!isFlush(player1.cards[index]) && !isFlush(player1.cards[index]))) {                            
                            if((isStraight(player1.cards[index]) && isStraight(player2.cards[index])) || (!isStraight(player1.cards[index]) && !isStraight(player2.cards[index]))) {                                
                                if((isThreeOfKind(player1.cards[index]) && isThreeOfKind(player2.cards[index])) || (!isThreeOfKind(player1.cards[index]) && !isThreeOfKind(player2.cards[index]))) {
                                    if((isTwoPairs(player1.cards[index]) && isTwoPairs(player2.cards[index])) || (!isTwoPairs(player1.cards[index]) && !isTwoPairs(player2.cards[index]))) {
                                        
                                        if((isOnePair(player1.cards[index]) && isOnePair(player2.cards[index])) || (!isOnePair(player1.cards[index]) && !isOnePair(player2.cards[index]))) {
                                  
                                            var comparator = isHighCard(player1.cards[index], player2.cards[index]);

                                            if(comparator.type == 1) {
                                                player1.isWin = '1';
                                                player1.combination = ENVIRONMENT.handsRanking.HIGH_CARD;
                                                $scope.isWinner.push(player1);
                                            }else if(comparator.type == 2) {
                                                player2.isWin = '2';
                                                player2.combination = ENVIRONMENT.handsRanking.HIGH_CARD;
                                                $scope.isWinner.push(player2);
                                            }else {
                                                $scope.isWinner.push({name: "Empate"});
                                            }
                                        } else {
                                            if(isOnePair(player1.cards[index]) && !isOnePair(player2.cards[index])) {
                                                player1.isWin = '1';
                                                player1.combination = ENVIRONMENT.handsRanking.ONE_PAIR;
                                                $scope.isWinner.push(player1);
                                            } else if(!isOnePair(player1.cards[index]) && isOnePair(player2.cards[index])) {
                                                player2.isWin = '2';
                                                player2.combination = ENVIRONMENT.handsRanking.ONE_PAIR;
                                                $scope.isWinner.push(player2);
                                            }
                                        }
                                    } else {
                                        if(isTwoPairs(player1.cards[index]) && !isTwoPairs(player2.cards[index])) {
                                            player1.isWin = '1';
                                            player1.combination = ENVIRONMENT.handsRanking.TWO_PAIRS;
                                            $scope.isWinner.push(player1);
                                        } else if(!isTwoPairs(player1.cards[index]) && isTwoPairs(player2.cards[index])) {
                                            player2.isWin = '2';
                                            player2.combination = ENVIRONMENT.handsRanking.TWO_PAIRS;
                                            $scope.isWinner.push(player2);
                                        }
                                    }
                                } else {
                                    if(isThreeOfKind(player1.cards[index]) && !isThreeOfKind(player2.cards[index])) {
                                        player1.isWin = '1';
                                        player1.combination = ENVIRONMENT.handsRanking.THREE_OF_A_KIND;
                                        $scope.isWinner.push(player1);
                                    } else if(!isThreeOfKind(player1.cards[index]) && isThreeOfKind(player2.cards[index])) {
                                        player2.isWin = '2';
                                        player2.combination = ENVIRONMENT.handsRanking.THREE_OF_A_KIND;
                                        $scope.isWinner.push(player2);
                                    }
                                } 
                            } else {
                                if(isStraight(player1.cards[index]) && !isStraight(player2.cards[index])) {
                                    player1.isWin = '1';
                                    player1.combination = ENVIRONMENT.handsRanking.STRAIGHT;
                                    $scope.isWinner.push(player1);
                                }else if(!isStraight(player1.cards[index]) && isStraight(player2.cards[index])) {
                                    player2.isWin = '2';
                                    player2.combination = ENVIRONMENT.handsRanking.STRAIGHT;
                                    $scope.isWinner.push(player2);
                                }
                            }

                        } else {
                            if(isFlush(player1.cards[index]) && !isFlush(player1.cards[index])) {
                                player1.isWin = '1';
                                player1.combination = ENVIRONMENT.handsRanking.FLUSH;
                                $scope.isWinner.push(player1);
                            } else if(!isFlush(player1.cards[index]) && isFlush(player1.cards[index])) {
                                player2.isWin = '2';
                                player2.combination = ENVIRONMENT.handsRanking.FLUSH;
                                $scope.isWinner.push(player2);
                            }
                        }
                    } else {
                        if(isFullHouse(player1.cards[index]) && !isFullHouse(player2.cards[index])) {
                            player1.isWin = '1';
                            player1.combination = ENVIRONMENT.handsRanking.FULL_HOUSE;
                            $scope.isWinner.push(player1);
                        } else if(!isFullHouse(player1.cards[index]) && isFullHouse(player2.cards[index])) {
                            player2.isWin = '2';
                            player2.combination = ENVIRONMENT.handsRanking.FULL_HOUSE;
                            $scope.isWinner.push(player2);
                        }
                    } 
                } else {
                    if(isFourOfKind(player1.cards[index]) && !isFourOfKind(player2.cards[index])) {
                        player1.isWin = '1';
                        player1.combination = ENVIRONMENT.handsRanking.FOUR_OF_A_KIND;
                        $scope.isWinner.push(player1);
                    }else if(!isFourOfKind(player1.cards[index]) && isFourOfKind(player2.cards[index])) {
                        player2.isWin = '2';
                        player2.combination = ENVIRONMENT.handsRanking.FOUR_OF_A_KIND;
                        $scope.isWinner.push(player2);
                    }
                }
            } else {
                if(isStraightFlush(player1.cards[index]) && !isStraightFlush(player2.cards[index])) {
                    player1.isWin = '1';
                    player1.combination = ENVIRONMENT.handsRanking.STRAIGHT_FLUSH;
                    $scope.isWinner.push(player1);
                } else if(!isStraightFlush(player1.cards[index]) && isStraightFlush(player2.cards[index])) {
                    player2.isWin = '2';
                    player2.combination = ENVIRONMENT.handsRanking.STRAIGHT_FLUSH;
                    $scope.isWinner.push(player2);
                }
            }
        } else {
            if(isRoyalFlush(player1.cards[index]) && !isRoyalFlush(player2.cards[index])) {
                player1.isWin = '1';
                player1.combination = ENVIRONMENT.handsRanking.ROYAL_FLUSH;
                $scope.isWinner.push(player1);
            } else if(!isRoyalFlush(player1.cards[index]) && isRoyalFlush(player2.cards[index])) {
                player2.isWin = '2';
                player2.combination = ENVIRONMENT.handsRanking.ROYAL_FLUSH;
                $scope.isWinner.push(player2);
            }
        }

    };



    // Regla 10
    function isRoyalFlush(cards) {
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;
        var suits = simpleArray.suits;

        if(numbers.indexOf(10) >= 0 &&
            numbers.indexOf(11) >= 0 && 
            numbers.indexOf(12) >= 0 &&
            numbers.indexOf(13) >= 0 && 
            numbers.indexOf(14) >= 0 &&
            sameSuit(suits)){
                return true;
        }

        return false;
    };
   
    // Regla 9
    function isStraightFlush(cards) {
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;
        var suits = simpleArray.suits;

        numbers.sort(function(a, b){
            return a - b;
        });

        var count = 0;
        for(var i = 0; i < numbers.length; i++) {

            if(((numbers[i] + 1) == numbers[i + 1]) || 
                (numbers[i] == 2 && numbers[i + 1] == 11) || 
                (numbers[i] == 3 && numbers[i + 1] == 12) ||                
                (numbers[i] == 4 && numbers[i + 1] == 13) ||
                (numbers[i] == 5 && numbers[i + 1] == 14)) {
                count++;
            } 
        }

        if((count == numbers.length - 1) && (countOccurrences(suits, suits[0]) == 5)) {
            return true;
        }

        return false;
    };

    // Regla 8
    function isFourOfKind(cards) {
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;

        for(var i = 0; i < numbers.length; i++) {
            if(countOccurrences(numbers, numbers[i]) == 4) {
                return true;
            }
        }

        return false;
    };

    // Regla 7
    function isFullHouse(cards) {
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;

        var unique = numbers.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });

        if(unique.length == 2) {
            if((countOccurrences(numbers, unique[0]) == 3 && countOccurrences(numbers, unique[1]) == 2) || 
                (countOccurrences(numbers, unique[0]) == 2 && countOccurrences(numbers, unique[1]) == 3)) {
                return true;
            }
        }

        return false;
    };

    // Regla 6
    function isFlush(cards) {
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;
        var suits = simpleArray.suits;

        if(countOccurrences(suits, suits[0]) == 5) {
            return true;
        }

        return false;
    };

    // Regla 5
    function isStraight(cards) {
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;
        var suits = simpleArray.suits;

        numbers.sort(function(a, b){
            return a - b;
        });

        var count = 0;
        for(var i = 0; i < numbers.length; i++) {

            if(((numbers[i] + 1) == numbers[i + 1]) || 
                (numbers[i] == 2 && numbers[i + 1] == 11) || 
                (numbers[i] == 3 && numbers[i + 1] == 12) ||                
                (numbers[i] == 4 && numbers[i + 1] == 13) ||
                (numbers[i] == 5 && numbers[i + 1] == 14)) {
                count++;
            } 
        }

        if(count == numbers.length - 1) {
            return true;
        }

        return false;
    };

    // Regla 4
    function isThreeOfKind(cards) { 
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;

        for(var i = 0; i < numbers.length; i++) {
            if(countOccurrences(numbers, numbers[i]) == 3) {
                return true;
            }
        }

        return false;
    };

    // Regla 3
    function isTwoPairs(cards) { 
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;

        var unique = numbers.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });

        if(unique.length == 3) {
            for(var i = 0; i < unique.length; i++) {
                if(countOccurrences(numbers, unique[i]) == 2) {
                    return true;
                }
            }
        }

        return false;
    };

    // Regla 2
    function isOnePair(cards) { 
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;

        var unique = numbers.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });

        if(unique.length == 4) {
            for(var i = 0; i < unique.length; i++) {
                if(countOccurrences(numbers, unique[i]) == 2) {
                    return true;
                }
            }
        }

        return false;
    };

    // Regla 1

    function isHighCard(cards1, cards2) {
        var c1 = evaluateArrays(cards1);
        var c2 = evaluateArrays(cards2);

        if(c1.toString() === c2.toString()) {
            return {type: 3};
        }

        for(var i = 0; i < c1.length; i++) {
            if(c1[i] > c2[i]){
                return {type: 1};
            }else{
                if(c1[i] < c2[i]){
                    return {type: 2};
                }
            }
        }


    };
    



    /**** UTILS ****/
    function countOccurrences(cards, card) {
        var count = 0;
        for(var i = 0; i < cards.length; i++) {
            if(cards[i] == card) {
                count++;
            }
        }

        return count;
    };

    function sameSuit(suits) {
        var count = 0;
        var firstCard = suits[0];

        for(var i = 1; i < suits.length; i++) { 
            if(firstCard == suits[i]) {
                count++;
            }
        }

        return count == (suits.length - 1);
    };

    function convertInOnlyNumbers(number) {
        switch (number) {
            case 'J' : return '11';
            break;
            case 'Q' : return '12';
            break;
            case 'K' : return '13';
            break;
            case 'A' : return '14';
            break;
            default : return number;
        }
    };

    function convertToSimpleArray(cards) {
        var numbers = [];
        var suits = [];
        for(var i = 0; i < cards.length; i++) {
            numbers.push(parseInt(convertInOnlyNumbers(cards[i].number)));
            suits.push(cards[i].suit);
        }

        return {numbers: numbers, suits: suits};
    };

    

    function evaluateArrays(cards) { 
        var simpleArray = convertToSimpleArray(cards);
        var numbers = simpleArray.numbers;
        numbers.sort(function(a, b){
            return b - a;
        });

        return numbers;
    };

    
});

'use strict';


angular.module('pokerApp')
  .controller('BaseCtrl', function($scope, Restangular, $state, $timeout) {

    $scope.number = 1;
    $scope.indexes = new Array($scope.number);
    $scope.isWinner = [];

    $scope.players = [
        { name: "Jugador 1", cards: [] },
        { name: "Jugador 2", cards: [] }

    ];

    loadToken();

    function loadToken() {
        Restangular.one('/dealer/deck').post()
            .then(function (response){
                console.log("Token obtenido correctamente");
                $scope.token = response;
                statusToken();
                return;

        }, function (response){            
            console.log("Error al obtener token");
            loadToken(); 
        });
    };

    $scope.restart = function(){
        $state.go('base.poker.play', {}, {reload: true});
    };

    function statusToken() {
        $timeout(function() {
            $scope.expiredToken = true;
        }, 300000); // 5 minutos
    }

    
});
